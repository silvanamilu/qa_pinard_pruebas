describe('Producto Simple POS', () => {

  beforeEach(() => {
    //archivo desde donde se toman los parametros
    cy.fixture('example.json').as('parametros');
    //asignacion de los parametros
    cy.get('@parametros').then(({ urlPos, usuPos, passPos }) => {
      cy.visit(urlPos);
      cy.get('#Username').type(usuPos);
      cy.get('#Password').type(passPos);
      cy.wait(2000);
      cy.get('.btn').click();
    });
  });


  //Caso de Prueba 1
  it('Crear Producto Simple', () => {
    cy.get(':nth-child(3) > [href="#"] > .sidebar-label').click().wait(2000);
    cy.get('.active > .second-level-nav > :nth-child(1) > a').click().wait(2000);

    //DATOS
    cy.get('#btn-create-new').click().wait(2000);
    cy.get('@parametros').then(({ prodPos , codBarrasPos }) => {
      cy.get('#Name').type(prodPos, { delay: 50 });
      cy.get(':nth-child(8) > div.col-md-7 > .selectize-control > .selectize-input').click();
      cy.get("#main-form > div:nth-child(8) > div > div > div.selectize-dropdown.single.form-control.mousetrap > div").click();
      cy.get(':nth-child(9) > div.col-md-7 > .selectize-control > .selectize-input').click().wait(1000);
      cy.get("#main-form > div:nth-child(9) > div > div > div.selectize-dropdown.single.form-control.mousetrap > div > div.option.active").click();
      cy.get('#BarCode').type(codBarrasPos,{delay:20});
      cy.get('.btn-primary').click().wait(3000);

    //PROVEEDORES
      cy.get('.text-center > .btn').click().wait(2000);
      cy.get('.data-row > :nth-child(1) > .form-control').select('ACTUALITY SRL', { delay: 20 });
      cy.get('.data-row > :nth-child(2) > .form-control').type(prodPos + '-cod', { delay: 20 });
      cy.get('.data-row > :nth-child(3) > .form-control').type(prodPos, { delay: 20 })
    });
    cy.get('.data-row > :nth-child(4) > .form-control').clear().type('50', { delay: 20 });
    cy.get('#suppliers-form > :nth-child(2) > :nth-child(1) > .card-container > .form-group > .col-xs-12 > .btn-primary').click().wait(4000);

    //PRECIOS
    cy.get('#SupplierPrice').clear().type('100', { delay: 20 }).wait(1000);
    cy.get('div.col-md-2 > .input-group > #ProfitMargin').clear().type('50', { delay: 20 }).wait(1000);
    cy.get(':nth-child(12) > div.col-md-2').click();
    cy.get('#FinalSalePrice').clear().type('200', { delay: 20 });
    cy.get('#price-analysis-form > :nth-child(16) > :nth-child(1) > .card-container > .form-group > .col-md-offset-3 > .btn-primary').click().wait(3000);

    //OFERTAS
    const todaysDate = Cypress.moment().format('DD/MM/YYYY')
    cy.log(todaysDate)
    cy.get('#StartDate').type(todaysDate, { delay: 20 });
    cy.get('#ExpiryDate').type(todaysDate, { delay: 20 });
    cy.get('#StockLimitQuantity').type('5', { delay: 20 });
    cy.get('#DiscountPercentage').clear().type('20', { delay: 20 });
    cy.get('div.col-sm-2 > .input-group > #FinalSalePrice').clear().type('150', { delay: 20 });
    cy.get(':nth-child(17) > :nth-child(1) > .card-container > .form-group > .col-md-offset-3 > .btn-primary').click();
    cy.wait(3000);

    //LISTA DE PRECIOS
    cy.get('#person-category-form > .form-group > .col-md-7 > .selectize-control > .selectize-input').click();
    cy.get('#person-category-form > div > div > div > div.selectize-dropdown.multi.mousetrap > div').first().click();
    cy.get('#person-category-form > .form-group > .col-sm-4').click().wait(1000);
    cy.get('[style="display: table-row;"] > .text-center > .apply-offer').click();
    cy.get('#btn-price-list-submit').click().wait(3000);

    //LOCALES
    cy.get('#btn-save-prices').click().wait(2000);

    //E-COMMERCE 
    //no hay nada
    //este es un comentario de prueba
    //Lorem ipsum dolor sit amet consectetur adipiscing elit montes, rutrum parturient scelerisque himenaeos ultrices
    // etiam turpis risus, convallis iaculis dis dui porta fames nam. Sem tincidunt maecenas congue eros sociis
    //
    cy.get('.card-tab-container > ul > :nth-child(1) > a').click();
    cy.get('#main-form > .row > :nth-child(1) > .card-container > .form-group > .col-md-offset-3 > .btn-default').click();
    //INDICE
    cy.get('#results').should('be.visible');
  })

  //Caso de Prueba 2
  it('Buscar Producto Simple', () => {
    cy.get(':nth-child(3) > [href="#"] > .sidebar-label').click().wait(2000);
    cy.get('.active > .second-level-nav > :nth-child(1) > a').click().wait(2000);
    cy.get('@parametros').then(({ prodPos }) => {
      cy.get('#searchPattern').type(prodPos, { delay: 50 });
      cy.get('.col-xs-4 > .btn-primary').click();
      cy.get('#results').contains(prodPos);
      cy.log('**EL PRODUCTO BUSCADO SI EXISTE**').wait(2000);
    });
    cy.get('#clean-button').click();
  })

  //Caso de Prueba 3
  it('Emilinar Producto Simple', () => {
    cy.get(':nth-child(3) > [href="#"] > .sidebar-label').click().wait(2000);
    cy.get('.active > .second-level-nav > :nth-child(1) > a').click().wait(2000);
    cy.get('@parametros').then(({ prodPos }) => {
      cy.get('#searchPattern').type(prodPos, { delay: 50 });
      cy.get('.col-xs-4 > .btn-primary').click();
      cy.get('#results').contains(prodPos);
      cy.log('**EL PRODUCTO BUSCADO SI EXISTE**').wait(2000);
    });
    cy.get('#results > table > tbody > tr > td:nth-child(10) > a:nth-child(2) > span').click()
    cy.get('.btn-danger').click();
  })

})