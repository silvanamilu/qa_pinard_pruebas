describe('Producto Simple POS', () => {

  beforeEach(() => {
    //archivo desde donde se toman los parametros
    cy.fixture('example.json').as('parametros');
    //asignacion de los parametros
    cy.get('@parametros').then(({ urlPos, usuPos, passPos }) => {
      cy.visit(urlPos);
      cy.get('#Username').type(usuPos);
      cy.get('#Password').type(passPos);
      cy.wait(2000);
      cy.get('.btn').click();
    });
  });

  //Caso de Prueba 1
  it.only('Crear Venta por Item', () => {

    cy.get(':nth-child(7) > [href="#"] > .sidebar-label').click();
    cy.get('.active > .second-level-nav > :nth-child(4) > a').click();
    //tipo de comprobante
    cy.get('#DocumentTypeId').select('FACTURA DE VENTA');
    cy.get('#FillingType').select('Por Item');

    cy.get('#create-by-document-submit').click().wait(3000);
    //se carga el comprobante
    cy.get('.col-xs-8 > :nth-child(3) > .col-xs-12 > :nth-child(1)').click();
    cy.get('#Comment').type('VENTA POR ITEM DE CONTADO CON CRYPRESS', { delay: 20 });

    cy.get('.allow-removed > :nth-child(2) > .form-control').click({force: true});
    cy.get('.allow-removed > :nth-child(2) > .form-control') .type('{enter}').wait(2000);
    cy.get('@parametros').then(({codBarrasPosVta }) => {
      cy.get('#search-product-form > .form-group > .col-xs-8 > .form-control').type(codBarrasPosVta, { delay: 20 });
      //cy.get('.allow-removed > :nth-child(2) > .form-control').type(codBarrasPos, { delay: 20 });
      //cy.get(':nth-child(11) > .col-sm-4').click();
      cy.get('#search-product-form > .form-group > .col-xs-4 > .btn-primary').click().wait(3000);
    });
    //falta cargar, hay bugs
    cy.get('#modal-search-product > .modal-dialog > .modal-content > .modal-footer > .pull-right > .btn-success').click();

  })

  //Caso de Prueba 2
  it('Buscar Venta', () => {

  })

})